import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import './assets/main.css'

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { faTwitter, faSpotify, faYoutube, faInstagram } from '@fortawesome/free-brands-svg-icons'
import { faVideo, faBullhorn, faPenNib, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { faPenToSquare } from '@fortawesome/free-regular-svg-icons'

/* add icons to the library */
library.add(faTwitter, faSpotify, faYoutube, faInstagram, faChevronRight, faVideo, faBullhorn, faPenNib, faPenToSquare)

const app = createApp(App)

app.component('font-awesome-icon', FontAwesomeIcon)

app.use(router)

app.mount('#app')
