// On page load or when changing themes, best to add inline in `head` to avoid FOUC
const lightSwitch = document.querySelector('#theme-switcher-light')
const darkSwitch = document.querySelector('#theme-switcher-dark')

if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
  document.documentElement.classList.add('dark');
  lightSwitch?.classList.remove('hidden')
  darkSwitch?.classList.add('hidden')
  
} else {
  document.documentElement.classList.remove('dark');
  lightSwitch?.classList.add('hidden')
  darkSwitch?.classList.remove('hidden')
};


function setDarkTheme() {
  document.documentElement.classList.add("dark");
  localStorage.theme = "dark";
  lightSwitch?.classList.remove('hidden')
  darkSwitch?.classList.add('hidden')
};

function setLightTheme() {
  document.documentElement.classList.remove("dark");
  localStorage.theme = "light";
  lightSwitch?.classList.add('hidden')
  darkSwitch?.classList.remove('hidden')
};

function onThemeSwitcherItemClick(event) {
  const theme = event.target.dataset.theme;

  if (theme === "system") {
    localStorage.removeItem("theme");
    setSystemTheme();
  } else if (theme === "dark") {
    setDarkTheme();
  } else {
    setLightTheme();
  }
};

const themeSwitcherItems = document.querySelectorAll('a[id^="theme-switcher"]');
themeSwitcherItems.forEach((item) => {
  item.addEventListener("click", onThemeSwitcherItemClick);
});