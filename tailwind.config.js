/** @type {import('tailwindcss').Config} */
module.exports = {
    darkMode: 'class',
    content: [
        "./index.html",
        "./src/**/*.{vue,js,ts,jsx,tsx}",
        "./node_modules/tw-elements/dist/js/**/*.js",
    ],
    theme: {

        colors: {
            'rt-primary': '#106a8c',
            'rt-secondary': '#b4c640',
            'rt-tertiary': '#1fbab6',
            'rt-gray': '#e6e6e6',
            transparent: 'transparent',
            white: '#fff',
            azul: {
                50: '#f0f9ff',
                100: '#dff2ff',
                200: '#b9e6fe',
                300: '#7bd4fe',
                400: '#34befc',
                500: '#0aa6ed',
                600: '#0085cb',
                700: '#006eab',
                800: '#055a87',
                900: '#0a4a70',
            },
            'surfie-green': {
                '50': '#ecfdff',
                '100': '#d0f6fd',
                '200': '#a6edfb',
                '300': '#69def7',
                '400': '#25c4eb',
                '500': '#09a8d1',
                '600': '#0b85af',
                '700': '#106a8c',
                '800': '#175873',
                '900': '#174962',
                '950': '#093043',
            },
            'earls-green': {
                '50': '#fafaeb',
                '100': '#f1f4d3',
                '200': '#e5ebab',
                '300': '#d0dc7a',
                '400': '#b4c640',
                '500': '#9db032',
                '600': '#7a8c24',
                '700': '#5d6b20',
                '800': '#4b561e',
                '900': '#40491e',
                '950': '#21280b',
            },
            'java': {
                '50': '#f1fcfb',
                '100': '#cff8f2',
                '200': '#9ef1e8',
                '300': '#66e2d9',
                '400': '#36cbc5',
                '500': '#1fbab6',
                '600': '#158c8c',
                '700': '#156f70',
                '800': '#15595a',
                '900': '#164b4b',
                '950': '#062b2d',
            },            
            'mercury': {
                '50': '#f8f8f8',
                '100': '#f0f0f0',
                '200': '#e6e6e6',
                '300': '#d1d1d1',
                '400': '#b4b4b4',
                '500': '#9a9a9a',
                '600': '#818181',
                '700': '#6a6a6a',
                '800': '#5a5a5a',
                '900': '#4e4e4e',
                '950': '#282828',
            },
        },
        extend: {},
    },
    plugins: [
        require("tw-elements/dist/plugin"),
        function({
            addBase,
            theme
        }) {
            function extractColorVars(colorObj, colorGroup = '') {
                return Object.keys(colorObj).reduce((vars, colorKey) => {
                    const value = colorObj[colorKey];

                    const newVars =
                        typeof value === 'string' ?
                        {
                            [`--color${colorGroup}-${colorKey}`]: value
                        } :
                        extractColorVars(value, `-${colorKey}`);

                    return {...vars,
                        ...newVars
                    };
                }, {});
            }

            addBase({
                ':root': extractColorVars(theme('colors')),
            });
        },
    ],
}